// L_19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>


class Animal
{
public:
    
   virtual void Voice()
    {
    }
   virtual ~Animal() {}
};

class Dog : public Animal
{
 public:
    void Voice() override
    {
        std::cout << "WOW \n";
    }
    ~Dog() {}
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow \n";
    }
    ~Cat() {}
};

class Duck : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Cria \n";
    }
    ~Duck() {}
};


int main()
{
    Dog* Song = new Dog;
    Cat* Song1 = new Cat;
    Duck* Song2 = new Duck;
    
    

    Animal* array[] { Song, Song1, Song2}; //{ Song->Voice(), Song1->Voice(), Song2->Voice()}; // � ����� ��� ����� �� �� ����������.
    
    for (int i = 0; i < 3; i++)
    {
         array[i]->Voice();
         delete array[i]; //����� ��������� ����� ���������� ������ � �����, ������ ������������ ��� ������� ���������.
    }
    
   // delete Song;
    //delete Song1;
   // delete Song2;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
