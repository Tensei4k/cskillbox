// L_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	 int d = 12;

	friend Vector operator*(const Vector& a, const int s);

	friend Vector operator-(const Vector& a, const Vector b);

	//friend bool operator> (const Vector& r, const Vector& b);

	friend std::ostream& operator<< (std::ostream& out, const Vector& e);

	friend std::istream& operator>> (std::istream& in, Vector& q);

private:
	float x;
	float y;
	float z;
};

Vector operator* (const Vector& a, const int s)
{
	return Vector(a.x * s, a.y * s, a.z * s);
}

Vector operator-(const Vector& a, const Vector b)
{
	if (Vector(a.z - a.x, b.d - a.z, a.y - b.x) > Vector(a.x - b.x, a.y - b.y, a.z - b.z))
	{
		return Vector(a.z - a.x, b.d - a.z, a.y - b.x);
	}
	Vector(a.z - a.x, b.d - a.z, a.y - b.x);
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}


//bool operator> (const Vector& r, const Vector& b)
//{
//	if (r > b)
//	{
//		return true;
//	}
//	if (b > r)
//	{
//		return false;
//	}
//}

std::ostream& operator<< (std::ostream& out, const Vector& e)
{
	out << ' ' << e.x << ' ' << e.y << ' ' << e.z;
	return out;
}
std::istream& operator>>(std::istream& in, Vector& q)
{
	in >> q.x;
	in >> q.y;
	in >> q.z;
	in >> q.d;
	return in;
}

int main()
{
	Vector v1(2, 5, 6);
	Vector v2(44, 32, 25);
	Vector v3;
	//Vector* w = Vector->d;
	//Vector w = Vector.d;
	//Vector w = Vector::d;
	int w = v1.d;
	
	//std::cin >> v1;
	//std::cout << v1 * w;
	std::cout << v1 - v2;
	//if (v3 > v2)
	//{
	//	std::cout << "Beee";
	//}
	
}

