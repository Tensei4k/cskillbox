// L_4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>

class Wheel
{
public:
	Wheel(float Diameter)
	{
	}
	float Diameter = 14.f;

};

class Engine
{
public:
    Engine(float EPower)
	{  
        Power = EPower;
	}
	float Power = 98.f;
};


class Vehicle
{
public:
    Vehicle() 
    {
      
    }
    
    virtual ~Vehicle(){ }

    
    Engine VEngine = VEngine;
    
    virtual std::ostream& Print(std::ostream& Out1) const
    {
        return Out1 << "Vehicle";
    }

    friend std::ostream& operator<<(std::ostream& Out2, Vehicle& q)
    {
        
        return q.Print(Out2);
    }
 
};

class WaterVehicle : public Vehicle
{
public:
    WaterVehicle(float WSediment)
    {
        Sediment = WSediment;
    }
    float Sediment = 100.f;
   

    std::ostream& Print(std::ostream& Out1) const override
    {
        Out1 << "Water Vehicle sediment is: " << Sediment;
        return Out1;
    }

};

class RoadVehicle : public Vehicle
{
public:
	RoadVehicle (){}
    float Clearance = 50.f;

};

class Bicycle : public RoadVehicle
{
public:
    
    Bicycle(Wheel BWheel1, Wheel BWheel2, int  BRide) 
    {
        Wheels[0] = BWheel1;
        Wheels[1] = BWheel2;
        Ride = BRide;
    }
    int Ride;
    std::vector <Wheel> Wheels{ (1), (2) };

	std::ostream& Print(std::ostream& Out1) const override
	{
        Out1 << "Bicycle  Wheels: " << Wheels[0].Diameter << ' ' << Wheels[1].Diameter << ' ' << "Ride height: " << Ride;
		return Out1;
	}


};

class Car : public RoadVehicle
{
public:
   

    Car(Engine CEngine, Wheel CWheel1, Wheel CWheel2, Wheel CWheel3, Wheel CWheel4, int VRide)
    {
        VEngine = CEngine;
        Wheels[0] = CWheel1;
        Wheels[1] = CWheel2;
        Wheels[2] = CWheel3;
        Wheels[3] = CWheel4;
        Ride = VRide;
    }
    std::vector <Wheel> Wheels{ (1), (2), (3), (4) };
    int Ride;

	std::ostream& Print(std::ostream& out1) const override
	{
		out1 << "Car engine: " << VEngine.Power << ' ' << "Wheels: "<< Wheels[0].Diameter << ' ' << Wheels[1].Diameter << ' ' << Wheels[2].Diameter << ' ' << Wheels[3].Diameter << ' ' << "Ride height: " << Ride;
		return out1;
	}


};
// maybe need put it to  class Vehicle
float GetHighestPower(std::vector<Vehicle*> Pv)
{
    float MaxEngine = 0;
    
    for (int Count = 0; Count < Pv.size(); Count++)
    {

        if (MaxEngine < Pv[Count]->VEngine.Power)
        {
            MaxEngine = Pv[Count]->VEngine.Power;
        }
    }
    return MaxEngine;
}


//Second part
int main()
{
    std::vector<Vehicle*> v;
    v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));
    v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));
    v.push_back(new WaterVehicle(5000));


    for (int Count = 0; Count < v.size(); ++Count)
    {
        std::cout << *v[Count] << '\n';
    }

    std::cout << "The highest power is: " << GetHighestPower(v) << '\n';// need to release this function

    // delete elements vector v here

	for (int Count = 0; Count < v.size(); ++Count)
	{
		 delete v[Count];
	}

}

//FirstPart

//int main()
//{
//    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
//    std::cout << c <<'\n';
//   
//    Bicycle t(Wheel(20), Wheel(20), 300);
//    std::cout << t << '\n';
//
//    return 0;
//}

